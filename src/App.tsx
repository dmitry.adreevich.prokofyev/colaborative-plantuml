import {Component, useState} from "react";
// @ts-ignore
import {getDiagramURIComponent} from "./uriEncoding";
// @ts-ignore
import AceEditor from "react-ace";

import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/ext-language_tools";

function App(){

  const [value, setValue] = useState("A->B");

  return (
      <div>
          <p>Collaborative PlantUml</p>
          <div className="editor">
        <Editor   setValue={setValue}  />
              </div>

        <Diagram  uri={value}></Diagram>
        </div>
  )
}

// // @ts-ignore
// function Form({value, setValue}){
//
//     const [internalValue, setInternalValue] = useState("A->B")
//   function handleChange(event: { target: { value: React.SetStateAction<string>; }; }) {
//         setInternalValue(event.target.value.toString());
//     setValue(getDiagramURIComponent(event.target.value.toString()))
//   }
//   function handleSubmit(event: { preventDefault: () => void; }) {
//         setValue(getDiagramURIComponent(internalValue))
//     event.preventDefault();
//   }
//
//   return (
//     <form onSubmit={handleSubmit}>
//       <label>
//         code:
//         <textarea value={internalValue} onChange={handleChange} />
//       </label>
//       <input type="submit" value="Submit" />
//     </form>
//   );
// }

class Diagram extends Component<{ uri: any }> {
    render() {
        let {uri} = this.props;
        return (
            <iframe title="frame" src={"http://localhost:8080/png/" + uri}></iframe>
        );
    }
}


// @ts-ignore
function Editor({setValue}){

function onChange(newValue: string) {
    setValue(getDiagramURIComponent(newValue))
}

// Render editor
return (
  <AceEditor
    mode="java"
    theme="github"
    onChange={onChange}
    name="UNIQUE_ID_OF_DIV"
    editorProps={{ $blockScrolling: true }}
  />
  // document.getElementById("example")
);
}


export default App;


